package main

import "github.com/xanzy/go-gitlab"

func ListGroupProjects(cli *gitlab.Client, group string) (map[string]*gitlab.Project, error) {
	projects := make(map[string]*gitlab.Project)
	page := 1
	for {
		options := &gitlab.ListGroupProjectsOptions{
			ListOptions: gitlab.ListOptions{
				Page: page,
			},
		}
		fetchedProjects, r, err := cli.Groups.ListGroupProjects(group, options, nil)
		if err != nil {
			return nil, err
		}
		for _, project := range fetchedProjects {
			projects[project.Path] = project
		}
		if r.NextPage <= 0 {
			break
		}
		page = r.NextPage
	}
	return projects, nil
}
