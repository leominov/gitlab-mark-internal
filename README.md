# gitlab-mark-internal

Переключить видимость всех проектов группы на Internal.

## Применение

```
Usage of ./gitlab-mark-internal:
  -b string
    	Gitlab Base URL (default "https://gitlab.qleanlabs.ru")
  -g string
    	Group name (default "qlean")
  -t string
    	Gitlab Token
```
