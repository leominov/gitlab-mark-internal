package main

import (
	"flag"
	"fmt"
	"os"

	"github.com/xanzy/go-gitlab"
)

var (
	group   = flag.String("g", "qlean", "Group name")
	token   = flag.String("t", "", "Gitlab Token")
	baseURL = flag.String("b", "https://gitlab.qleanlabs.ru", "Gitlab Base URL")
)

func main() {
	flag.Parse()

	cli, err := gitlab.NewClient(*token, gitlab.WithBaseURL(*baseURL))
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	projects, err := ListGroupProjects(cli, *group)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	for _, project := range projects {
		if project.Visibility == gitlab.InternalVisibility {
			continue
		}
		fmt.Println(project.Name)
		_, _, err = cli.Projects.EditProject(project.ID, &gitlab.EditProjectOptions{
			Visibility: gitlab.Visibility(gitlab.InternalVisibility),
		})
		if err != nil {
			fmt.Printf("failed to update: %v\n", err)
		}
	}
}
